package fibonacci;

import java.util.Scanner;

public class FibonacciSeries {

	public static long validateN(String s) {
		try {
			long num = Integer.parseInt(s);
			if (num > 0)
				return num;
		} catch (NumberFormatException e) {
			System.out.println("Invalid Number");
		}
		return 0;
	}

	public static long calculateNthValue(long input_num) {
		if (input_num == 1 || input_num == 2)
			return 1;
		long n_1= 1;
		long n_2= 1;
		long n= 2;
		long result= 0;
		do
		{
			result= n_1 + n_2;
			n_2= n_1;
			n_1= result;
			n++;
		}
		while (n < input_num);
		return result;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		long input_num = 0;
		do {
			System.out.println("Enter number");
			String input = in.nextLine();
			input_num = validateN(input);
			System.out.println(calculateNthValue(input_num));
		} while (input_num > 0);
		in.close();
	}
}
