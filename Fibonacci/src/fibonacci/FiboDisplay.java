package fibonacci;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FiboDisplay {

	private JFrame frame;
	private JTextField textField_input;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FiboDisplay window = new FiboDisplay();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FiboDisplay() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 661, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEnterNumber = new JLabel("Enter Number");
		lblEnterNumber.setBounds(103, 122, 166, 15);
		frame.getContentPane().add(lblEnterNumber);
		
		textField_input = new JTextField();
		textField_input.setBounds(376, 120, 114, 19);
		frame.getContentPane().add(textField_input);
		textField_input.setColumns(10);
		
		JLabel lblResult = new JLabel("");
		lblResult.setBounds(67, 313, 550, 15);
		frame.getContentPane().add(lblResult);
		
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				long l= FibonacciSeries.validateN(textField_input.getText());
				if (l > 0)
				{
					lblResult.setText("Result is "+FibonacciSeries.calculateNthValue(l));
				}
				else
					lblResult.setText("Invalid Input Detected , please use positive integer");
			}
		});
		btnCalculate.setBounds(217, 208, 117, 25);
		frame.getContentPane().add(btnCalculate);
		

	}
}
